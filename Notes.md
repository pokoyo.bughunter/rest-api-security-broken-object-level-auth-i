Broken-Object-Level-Auth-I 


Securing an API is a critical task. A poor API design may lead to system compromise. Its impact becomes more critical when a crucial service gets affected like a Banking API.

The Banking WebApp provides its customers, the ability to view their balances. The authorization system is not implemented properly and allows any user to read the account balance of all the other users.

This challenge is focused on leveraging the Broken Object Level Authorization in the Banking API.

Objective: Retrieve the account balance of the user named "James Cooper"!


User Information:
Username	elliot
Password    elliotalderson

API Endpoints:
Endpoint Description
	Method
	Parameter(s)
/issue
	Issue a JWT Token used as an authorization mechanism
	POST
	identifier, password
/balance
	Read the balance of an account
	GET
	acct, token

Instructions: 

    This lab is dedicated to you! No other users are on this network :)
    Once you start the lab, you will have access to a Kali GUI instance.
    Your Kali instance has an interface with IP address 192.X.Y.2. Run "ifconfig" to know the values of X and Y.
    The Banking WebApp is running on port 5000 on the target machine having IP address 192.X.Y.3.
    The account number is a 4-digit number, ranging from 1000 to 9999 inclusive.
    Do not attack the gateway located at IP address 192.X.Y.1

[Prohibited Activities]

    The following activities are strictly prohibited on this website unless otherwise explicitly stated as allowed in the mission statement:

    Using automated scanners
    Using brute force attacks
    Denial of Service attacks
    Attacking other student machines in challenges where you might achieve a shell on the vulnerable system
    Attacking the lab infrastructure

Users violating the above will be either temporarily or permanently banned from the website. 

If you are unsure about an activity, then please contact support to confirm that it is allowed on our website.


* Technical Support for this Lab:

There is a reason we provide unlimited lab time: you can take as much time as you need to solve a lab. However, we realize that sometimes hints might be necessary to keep you motivated!

We currently provide technical support limited to:

    Giving hints for a lab exercise
    In rare circumstances, if you have totally given up (NO!!!) then tell you how to solve it. This will be limited to sharing the solution video or lab report
    A lab exercise fails to load or has errors in it

If you need technical support, please email  attackdefense@pentesteracademy.com  clearly mention the name and link of the lab exercise and other essential details. The more descriptive you are, the faster we can help you. We will get back to you within 24 hours or less. 

For adminitrative queries, billing, enterprise accounts etc. please email feedback@binarysecuritysolutions.com 


https://www.attackdefense.com/challengedetails?cid=1916